import sys
import os
import random

from kivy.app import App
from kivy.resources import resource_add_path
from kivy.properties import ObjectProperty, NumericProperty
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.uix.widget import Widget


class GameField(object):
    def __init__(self):
        self.data = [0 for _ in range(16)]
    
    """na starcie na planszy pojawiają się dwa pola z losową liczbą 3 lub 6"""
    def add_tile(self):
        indices = [i for i, v in enumerate(self.data) if v == 0]
        self.data[random.choice(indices)] = random.choice((3, 6))

    def __getitem__(self, i):
        return self.data[i]

    def __iter__(self):
        return iter(self.data)

    def rows(self):
        return [
            self.data[i*4:i*4+4] for i in range(4)
        ]

    def from_rows(self, rows):
        changed = False
        for i, row in enumerate(rows):
            changed |= any(
                a != b for a, b in zip(row, self.data[i*4:i*4+4]))
            if changed:
                self.data[i*4:i*4+4] = row
        return changed
    
    """opis planszy, która składa się z 16 kwadratów"""
    def cols(self):
        return [
            [
                self.data[i],
                self.data[4+i],
                self.data[8+i],
                self.data[12+i]
            ] for i in range(4)
        ]

    def from_cols(self, cols):
        changed = False
        for i, col in enumerate(cols):
              changed |= any(
                  a != b for a, b in zip(col, [
                  self.data[i],
                  self.data[4+i],
                  self.data[8+i],
                  self.data[12+i]]))
              if changed:
                  self.data[i] = col[0]
                  self.data[4+i] = col[1]
                  self.data[8+i] = col[2]
                  self.data[12+i] = col[3]
        return changed
    
    """kod do łączenia kwadratów z takimi samymi liczbami na planszy """
    def shift_line(self, line):
        line = [v for v in line if v != 0]
        i = 0
        previous = 0
        while i < len(line):
            if previous != 0 and previous == line[i]:
                line[i-1] *= 2
                del line[i]
                previous = 0
            else:
                previous = line[i]
                i += 1

        while len(line) < 4:
            line.append(0)

        return line
   
    """kod do poruszania kwadratami na planszy"""
    def right(self):
        return self.from_rows([
            self.shift_line(row)
        for row in self.rows()])

    def left(self):
        return self.from_rows([list(reversed(
            self.shift_line(list(reversed(row)))))
        for row in self.rows()])

    def down(self):
        return self.from_cols([
            self.shift_line(col)
            for col in self.cols()])

    def up(self):
        return self.from_cols([list(reversed(
            self.shift_line(list(reversed(col)))))
            for col in self.cols()])

class Tile(Widget):
    value = NumericProperty(0)
    def __init__(self, **kwargs):
        super(Tile, self).__init__(**kwargs)

""" kod odpowiadający za poprawne wyświetlania planszy oraz poruszanie sie"""
class Field(FocusBehavior, Widget):
    field = ObjectProperty(None)
    grid = ObjectProperty(None)
    def __init__(self, **kwargs):
        super(Field, self).__init__(**kwargs)

    def on_grid(self, old, new):
        for v in self.parent.field:
            self.grid.add_widget(Tile(value=v))

    def keyboard_on_key_up(self, window, code):
        if code[1] in ('left', 'right', 'up', 'down'):
             if getattr(self.field, code[1])():
                 self.update()

    def update(self):
        self.field.add_tile()
        for i, v in enumerate(self.field):
            self.grid.children[i].value = v

class Viewport(Widget):
    def __init__(self, field, **kwargs):
        self.field = field
        super(Viewport, self).__init__(**kwargs)

    def on_parent(self, widget, parent):
        self.children[0].focus = True

class Game3072App(App):
    def __init__(self, **kwargs):
        super(Game3072App, self).__init__(**kwargs)
        self.field = GameField()
        self.field.add_tile()
        self.field.add_tile()

    def build(self):
        viewport = Viewport(self.field)
        return viewport


if __name__ == '__main__':
    resource_add_path(os.path.dirname(os.path.abspath(__file__)))
    sys.exit(Game3072App().run())

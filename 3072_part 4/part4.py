import sys
import os
import random

from kivy.app import App
from kivy.resources import resource_add_path
from kivy.properties import ObjectProperty, NumericProperty
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.uix.widget import Widget
from kivy.uix.label import Label

class GameField(object):
    def __init__(self):
        self.data = [0 for _ in range(16)]

    def add_tile(self):
        indices = [i for i, v in enumerate(self.data) if v == 0]
        self.data[random.choice(indices)] = random.choice((3, 6))

    def __getitem__(self, i):
        return self.data[i]

    def __iter__(self):
        return iter(self.data)

    def rows(self):
        return [
            self.data[i*4:i*4+4] for i in range(4)
        ]

    def from_rows(self, rows):
        changed = False
        for i, row in enumerate(rows):
            changed |= any(
                a != b for a, b in zip(row, self.data[i*4:i*4+4]))
            if changed:
                self.data[i*4:i*4+4] = row
        return changed

    def cols(self):
        return [
            [
                self.data[i],
                self.data[4+i],
                self.data[8+i],
                self.data[12+i]
            ] for i in range(4)
        ]

    def from_cols(self, cols):
        changed = False
        for i, col in enumerate(cols):
              changed |= any(
                  a != b for a, b in zip(col, [
                  self.data[i],
                  self.data[4+i],
                  self.data[8+i],
                  self.data[12+i]]))
              if changed:
                  self.data[i] = col[0]
                  self.data[4+i] = col[1]
                  self.data[8+i] = col[2]
                  self.data[12+i] = col[3]
        return changed
class Tile(Widget):
    value = NumericProperty(0)
    def __init__(self, **kwargs):
        super(Tile, self).__init__(**kwargs)

        
class Viewport(Widget):
    def __init__(self, field, **kwargs):
        self.field = field
        super(Viewport, self).__init__(**kwargs)

    def on_parent(self, widget, parent):
        self.children[0].focus = True
        
class Field(FocusBehavior, Widget):
    field = ObjectProperty(None)
    grid = ObjectProperty(None)
    def __init__(self, **kwargs):
        super(Field, self).__init__(**kwargs)

    def on_grid(self, old, new):
        for v in self.parent.field:
            self.grid.add_widget(Tile(value=v))

    def keyboard_on_key_up(self, window, code):
        if code[1] in ('left', 'right', 'up', 'down'):
             if getattr(self.field, code[1])():
                 self.update()

    def update(self):
        self.field.add_tile()
        for i, v in enumerate(self.field):
            self.grid.children[i].value = v

class Game3072App(App):
    def __init__(self, **kwargs):
        super(Game3072App, self).__init__(**kwargs)
        self.field = GameField()
        self.field.add_tile()
        self.field.add_tile()
    def build(self):
        viewport = Viewport(self.field)
        return viewport


if __name__ == '__main__':
    resource_add_path(os.path.dirname(os.path.abspath(__file__)))
    sys.exit(Game3072App().run())
